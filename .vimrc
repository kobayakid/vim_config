:nmap \l :setlocal number!<CR>
:nmap \o :set paste!<CR>
:nmap j gj
:nmap k gk
:set incsearch
:set ignorecase
:set smartcase
:set hlsearch
:nmap \q :nohlsearch<CR>
set term=xterm-256color
" Pathogen
" filetype off " Pathogen needs to run before plugin indent on
" call pathogen#runtime_append_all_bundles()
" call pathogen#helptags() " generate helptags for everything in 'runtimepath'
" filetype plugin indent on
call pathogen#infect()
call pathogen#helptags()
autocmd vimenter * if !argc() | NERDTree | endif
syntax on
filetype plugin indent on
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
set t_Co=256
:nmap \t :set expandtab tabstop=4 shiftwidth=4 softtabstop=4<CR>
:nmap \T :set expandtab tabstop=8 shiftwidth=8 softtabstop=4<CR>
:nmap \M :set noexpandtab tabstop=8 softtabstop=4 shiftwidth=4<CR>
:nmap \m :set expandtab tabstop=2 shiftwidth=2 softtabstop=2<CR>
:nmap \w :setlocal wrap!<CR>:setlocal wrap?<CR>
:colorscheme distinguished
:filetype plugin indent on
